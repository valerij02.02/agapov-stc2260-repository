package Test.controller;

import Test.dto.UserDTO;
import Test.service.file.UserCheck;
import Test.service.file.UserFileService;
import Test.service.file.UserFileServiceImpl;
import Test.service.userService.UserService;
import Test.service.userService.UserServiceImpl;

import java.util.List;

public class Controller {

    UserFileService ufsi;
    UserService us;

    public Controller() {
        ufsi = new UserFileServiceImpl();
        us = new UserServiceImpl(ufsi);
    }

    public int checkFile() {
        return ufsi.checkFile();
    }

    public void createNewFile() {
        ufsi.createNewFile();
    }

    public List<UserDTO> readFileOfUsers() {
        return ufsi.readFileOfUsersDTO();
    }

    public void checkUserUnique(List<UserDTO> users) {
        UserCheck.checkUserUnique(users);
    }

    public UserDTO findUserById(int id) {
        return us.findById(id);
    }

    public void createUser(UserDTO userDTO) {
        us.create(userDTO);
    }

    public void updateUser(UserDTO userDTO) {
        us.update(userDTO);
    }

    public void deleteUserById(int id) {
        us.delete(id);
    }
}