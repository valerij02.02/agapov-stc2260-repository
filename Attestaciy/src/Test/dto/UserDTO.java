package Test.dto;
import java.util.Objects;

public class UserDTO implements Comparable<UserDTO> {
    int id;
    String firstName;
    String lastName;
    int age;
    boolean hasWork;

    public UserDTO(int id, String firstName, String lastName, int age, boolean hasWork) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hasWork = hasWork;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public boolean isHasWork() {
        return hasWork;
    }

    public void setHasWork(boolean hasWork) {
        this.hasWork = hasWork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO userDTO = (UserDTO) o;
        return id == userDTO.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id +
                "|" + firstName +
                '|' + lastName +
                '|' + age +
                "|" + hasWork;
    }

    @Override
    public int compareTo(UserDTO o) {
        return Integer.compare(this.getId(), o.getId());
    }
}