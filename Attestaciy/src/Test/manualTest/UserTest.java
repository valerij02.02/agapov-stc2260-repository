package Test.manualTest;

import Test.controller.Controller;
import Test.dto.UserDTO;
import Test.service.userFactory.UserFactoryImpl;

import java.util.List;

public class UserTest {

    public void start() {
        Controller controller = new Controller();
        UserFactoryImpl userFactoryImpl = new UserFactoryImpl();
        List<UserDTO> users;

        System.out.println("Проверка файла...");
        int checkFile = controller.checkFile();

        try {
            Thread.sleep(1000);
            if (checkFile != 0) {
                if (checkFile == 1) {
                    System.out.println("Файл пуст!");
                    System.out.println("Запись файла");
                } else if (checkFile == -1) {
                    System.out.println("Файл не найден!");
                    System.out.println("Создание нового файла с базовыми параметрами...");
                }
                controller.createNewFile();
            }
            System.out.println("Сканикование файла");
            users = controller.readFileOfUsers();
            controller.checkUserUnique(users);
            Thread.sleep(1000);
        } catch (
                InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println();
        users = controller.readFileOfUsers();

        printUsers(users);

        System.out.println("Поиск пользователя 5: " + controller.findUserById(5));
        System.out.println("=============================");

        System.out.println("Поиск не существующего пользователя 25: " + controller.findUserById(25));
        System.out.println("=============================");

        System.out.println("\nОбновление пользователя 2");
        controller.updateUser(new UserDTO(2,
                userFactoryImpl.generateFirstName(),
                userFactoryImpl.generateLastName(),
                20,
                false));
        users = controller.readFileOfUsers();

        printUsers(users);

        System.out.println("\nОбновлени не найденого пользователя 32");
        controller.updateUser(new UserDTO(32,
                "Not",
                "Valid",
                30,
                true));
        users = controller.readFileOfUsers();

        printUsers(users);

        System.out.println("\nУдаление пользователя 5");
        controller.deleteUserById(5);
        users = controller.readFileOfUsers();

        printUsers(users);

        System.out.println("\nУдаление не найденого пользователя 15");
        controller.deleteUserById(5);
        users = controller.readFileOfUsers();

        printUsers(users);

        System.out.println("\nСоздание пользователя 3");
        controller.createUser(new UserDTO(3,
                userFactoryImpl.generateFirstName(),
                userFactoryImpl.generateLastName(),
                40,
                true));
        users = controller.readFileOfUsers();

        printUsers(users);

        System.out.println("\nСоздание не найденого пользователя 12");
        controller.createUser(new UserDTO(12,
                userFactoryImpl.generateFirstName(),
                userFactoryImpl.generateLastName(),
                40,
                true));
        users = controller.readFileOfUsers();

        printUsers(users);

        System.out.println("\nСоздание ранее удалённого пользователя 5");
        controller.createUser(new UserDTO(5,
                userFactoryImpl.generateFirstName(),
                userFactoryImpl.generateLastName(),
                15,
                false));
        users = controller.readFileOfUsers();

        printUsers(users);
    }

    private static void printUsers(List<UserDTO> users) {
        System.out.println("========== Пользователи: ===========");
        for (UserDTO user : users) {
            System.out.println(user);
        }
        System.out.println("=============================");
    }
}