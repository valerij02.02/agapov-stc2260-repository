package Test.model;

import java.util.HashSet;
import java.util.Set;

public class Ids {
    public static final Ids INSTANCE = new Ids();
    private final Set<Integer> currentIds = new HashSet<>();
    private final Set<Integer> deletedIds = new HashSet<>();

    private Ids() {}

    public Set<Integer> getCurrentIds() {
        return currentIds;
    }

    public Set<Integer> getDeletedIds() {
        return deletedIds;
    }
}