package Test.model;

import java.util.Objects;

public class User implements Comparable<User> {
    int id;
    String firstName;
    String lastName;
    int age;
    boolean hasWork;

    public User(int id, String firstName, String lastName, int age, boolean hasWork) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hasWork = hasWork;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isHasWork() {
        return hasWork;
    }

    public void setHasWork(boolean hasWork) {
        this.hasWork = hasWork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id +
                "|" + firstName +
                '|' + lastName +
                '|' + age +
                "|" + hasWork;
    }

    @Override
    public int compareTo(User o) {
        return Integer.compare(this.getId(), o.getId());
    }
}