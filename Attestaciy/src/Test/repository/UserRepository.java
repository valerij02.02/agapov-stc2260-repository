package Test.repository;

import Test.model.User;

public interface UserRepository {
    User findById(int id);
    void create(User user);
    void update(User user);
    void delete(int id);
}