package Test.repository;

import Test.model.User;
import Test.service.file.UserFileService;
import Test.service.id.IdsServiceImpl;

import java.util.Collections;
import java.util.List;

public class UserRepositoryFileImpl implements UserRepository {

    UserFileService usfi;
    IdsServiceImpl idsSI;

    public UserRepositoryFileImpl(UserFileService usfi) {
        this.usfi = usfi;
        idsSI = new IdsServiceImpl();
    }

    @Override
    public User findById(int id) {
        List<User> users = usfi.readFileOfUsers();
        User user = users.stream()
                .filter(n -> n.getId() == id)
                .findFirst()
                .orElse(null);
        if (user == null) {
            System.out.println("User not found");
        }
        return user;
    }

    @Override
    public void create(User user) {
        if (idsSI.checkId(user.getId())) {
            List<User> users = usfi.readFileOfUsers();
            idsSI.setId(user.getId());
            users.add(user);
            Collections.sort(users);
            usfi.updateFile(users);
        }
    }

    @Override
    public void update(User user) {
        List<User> users = usfi.readFileOfUsers();
        if (this.findById(user.getId()) == null)
            return;
        users.set(users.indexOf(user), user);
        usfi.updateFile(users);
    }

    @Override
    public void delete(int id) {
        List<User> users = usfi.readFileOfUsers();
        try {
            users.remove(this.findById(id));
            idsSI.removeId(id);
        } catch (Exception e) {
            System.err.println("Can't remove user");
        }
        usfi.updateFile(users);
    }
}