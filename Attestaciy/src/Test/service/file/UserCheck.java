package Test.service.file;

import Test.dto.UserDTO;
import Test.model.User;
import Test.repository.UserRepository;
import Test.repository.UserRepositoryFileImpl;
import Test.service.mapper.UserMapper;

import java.util.*;

public class UserCheck {

    public static void checkUserUnique(List<UserDTO> usersDTO) {
        Map<Integer, Integer> idsMap = new HashMap<>();
        List<User> users = UserMapper.mapToUserList(usersDTO);

        for (User user : users) {
            if (!idsMap.containsKey(user.getId())) {
                idsMap.put(user.getId(), 1);
            } else {
                idsMap.replace(user.getId(), idsMap.get(user.getId()) + 1);
            }
        }

        for (Map.Entry<Integer, Integer> userId : idsMap.entrySet()) {
            if (userId.getValue() != 1) {
                requestForDeleteUser(users, userId.getKey());
            }
        }
    }

    private static void requestForDeleteUser(List<User> users, Integer userId) {
        Scanner scanner = new Scanner(System.in);
        List<User> usersForDelete = new ArrayList<>();

        for (User user : users) {
            if (user.getId() == userId) {
                usersForDelete.add(user);
            }
        }

        System.err.println("Error: Duplicates founded:");
        for (int i = 0; i < usersForDelete.size(); i++) {
            System.out.println("[" + (i + 1) + "] " + usersForDelete.get(i));
        }
        System.out.println("Please, input which one you want to save (just one unique user allowed)");

        User savedUser = usersForDelete.get(scanner.nextInt() - 1);

        UserFileService ufsi = new UserFileServiceImpl();
        UserRepository urfi = new UserRepositoryFileImpl(ufsi);
        for (User user: usersForDelete) {
            urfi.delete(user.getId());
        }
        urfi.create(savedUser);
    }
}