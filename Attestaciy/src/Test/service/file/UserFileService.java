package Test.service.file;


import Test.dto.UserDTO;
import Test.model.User;

import java.util.List;

public interface UserFileService {
    int checkFile();
    void createNewFile();
    List<User> readFileOfUsers();
    List<UserDTO> readFileOfUsersDTO();
    void updateFile(List<User> users);
}