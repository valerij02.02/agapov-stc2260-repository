package Test.service.file;

import Test.dto.UserDTO;
import Test.model.User;
import Test.service.id.IdsServiceImpl;
import Test.service.mapper.UserMapper;
import Test.service.userFactory.UserFactoryImpl;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class UserFileServiceImpl implements UserFileService{

    IdsServiceImpl idsSI = new IdsServiceImpl();
    final int USERS_SIZE = 5;
    static final String PROPERTIES_PATH = "src/Test/db.properties";
    static final String FILE_PROPERTY = "PATH_TO_FILE";
    File file = new File(getFilePath());

    @Override
    public int checkFile() {
        if (!file.exists()) {
            return -1;
        } else if (file.length() == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void createNewFile() {
        List<User> newUsers = new LinkedList<>();

        UserFactoryImpl userFactoryImpl = new UserFactoryImpl();
        for (int i = 0; i < USERS_SIZE; i++) {
            newUsers.add(new User(idsSI.generateId(),
                    userFactoryImpl.generateFirstName(),
                    userFactoryImpl.generateLastName(),
                    userFactoryImpl.generateAge(),
                    userFactoryImpl.generateHasWork()));
        }
        this.updateFile(newUsers);
    }

    @Override
    public List<User> readFileOfUsers() {
        List<User> users;

        try {
            users = Files.readAllLines(Path.of(getFilePath())).stream()
                    .map(line -> {
                        String[] userInfo = line.split("\\|");
                        idsSI.setId(Integer.parseInt(userInfo[0]));
                        int id = Integer.parseInt(userInfo[0]);
                        int age = Integer.parseInt(userInfo[3]);
                        boolean isWorker = Boolean.parseBoolean(userInfo[4]);
                        return new User(id, userInfo[1], userInfo[2], age, isWorker);
                    }).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return users;
    }

    @Override
    public List<UserDTO> readFileOfUsersDTO() {
        return UserMapper.mapToUserDtoList(readFileOfUsers());
    }


    @Override
    public void updateFile(List<User> users) {
        try (Writer fileWriter = new BufferedWriter(new FileWriter(file, false))) {
            for (User user : users) {
                fileWriter.write(user.toString());
                fileWriter.write('\n');
            }

            fileWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getFilePath() {
        Properties properties = new Properties();
        try (FileInputStream fileInputStream
                     = new FileInputStream(PROPERTIES_PATH)) {
            properties.load(fileInputStream);
            return properties.getProperty(FILE_PROPERTY);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}