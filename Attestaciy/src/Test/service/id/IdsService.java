package Test.service.id;


public interface IdsService {
    int generateId();
    void setId(int newId);
    void removeId(int id);
    boolean checkId(int id);
}