package Test.service.id;

import Test.model.Ids;

import java.util.Collections;

public class IdsServiceImpl implements IdsService {
    Ids ids = Ids.INSTANCE;

    @Override
    public int generateId() {
        if (ids.getCurrentIds().isEmpty()) {
            ids.getCurrentIds().add(1);
            return 1;
        } else if (!ids.getDeletedIds().isEmpty()) {
            int minIdFromDeleted = Collections.min(ids.getDeletedIds());
            ids.getDeletedIds().remove(minIdFromDeleted);
            return minIdFromDeleted;
        } else {
            int newId = Collections.max(ids.getCurrentIds()) + 1;
            ids.getCurrentIds().add(newId);
            return newId;
        }
    }

    @Override
    public void setId(int newId) {
        ids.getDeletedIds().remove(newId);
        ids.getCurrentIds().add(newId);
    }

    @Override
    public void removeId(int id) {
        ids.getDeletedIds().add(id);
        ids.getCurrentIds().remove(id);
    }

    @Override
    public boolean checkId(int id) {
        if (ids.getCurrentIds().contains(id)) {
            System.out.println("Current id already in use!");
            return false;
        }
        return true;
    }
}