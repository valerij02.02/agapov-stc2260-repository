package Test.service.mapper;

import Test.dto.UserDTO;
import Test.model.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {

    public static User map(UserDTO userDTO) {
        if (userDTO == null)
            return null;
        return new User(
                userDTO.getId(),
                userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getAge(),
                userDTO.isHasWork()
        );
    }

    public static UserDTO map(User user) {
        if (user == null)
            return null;
        return new UserDTO(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getAge(),
                user.isHasWork()
        );
    }

    public static List<User> mapToUserList(List<UserDTO> usersDto) {
        return usersDto.stream()
                .map(UserMapper::map)
                .collect(Collectors.toList());
    }

    public static List<UserDTO> mapToUserDtoList(List<User> users) {
        return users.stream()
                .map(UserMapper::map)
                .collect(Collectors.toList());
    }
}