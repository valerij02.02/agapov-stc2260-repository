package Test.service.userFactory;


public interface UserFactory {
    String generateFirstName();
    String generateLastName();
    int generateAge();
    boolean generateHasWork();
}