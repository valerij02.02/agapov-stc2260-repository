package Test.service.userFactory;

import java.util.Random;

public class UserFactoryImpl implements UserFactory {
    private final String[] firstNames = {"Агапов", "Александр", "Сергей", "Василий", "Констнтин", "Семён"};
    private final String[] lastNames = {"Кирилович", "Вячесловович", "Васильевич", "Василий", "Сергеевич", "Александрович"};
    Random random = new Random();
    int age;

    @Override
    public String generateFirstName() {
        return firstNames[random.nextInt(firstNames.length - 1)];
    }

    @Override
    public String generateLastName() {
        return lastNames[random.nextInt(lastNames.length - 1)];
    }

    @Override
    public int generateAge() {
        return age = random.nextInt(100);
    }

    @Override
    public boolean generateHasWork() {
        if (age > 13 && age < 19) {
            return random.nextInt(5) == 3;
        } else if (age >= 19 && age < 60) {
            return !(random.nextInt(5) == 3);
        } else if (age >= 60 && age < 80) {
            return random.nextInt(4) == 3;
        }
        return false;
    }
}
