package Test.service.userService;

import Test.dto.UserDTO;
import Test.model.User;

public interface UserService {
    UserDTO findById(int id);
    void create(UserDTO user);
    void update(UserDTO user);
    void delete(int id);
    User convertToUser(UserDTO userDTO);
    UserDTO convertToUserDTO(User user);
}