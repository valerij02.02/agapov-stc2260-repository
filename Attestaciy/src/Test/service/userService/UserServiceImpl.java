package Test.service.userService;

import Test.dto.UserDTO;
import Test.model.User;
import Test.repository.UserRepository;
import Test.repository.UserRepositoryFileImpl;
import Test.service.file.UserFileService;
import Test.service.mapper.UserMapper;

public class UserServiceImpl implements UserService {

    UserFileService ufsi;
    UserRepository urfi;

    public UserServiceImpl(UserFileService ufsi) {
        this.ufsi = ufsi;
        this.urfi = new UserRepositoryFileImpl(ufsi);
    }

    @Override
    public UserDTO findById(int id) {
        return convertToUserDTO(urfi.findById(id));
    }

    @Override
    public void create(UserDTO userDTO) {
        urfi.create(convertToUser(userDTO));
    }

    @Override
    public void update(UserDTO userDTO) {
        urfi.update(convertToUser(userDTO));
    }

    @Override
    public void delete(int id) {
        urfi.delete(id);
    }

    @Override
    public User convertToUser(UserDTO userDTO) {
        return UserMapper.map(userDTO);
    }

    @Override
    public UserDTO convertToUserDTO(User user) {
        return UserMapper.map(user);
    }
}