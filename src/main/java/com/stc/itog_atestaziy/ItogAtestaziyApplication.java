package com.stc.itog_atestaziy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItogAtestaziyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItogAtestaziyApplication.class, args);
    }

}
