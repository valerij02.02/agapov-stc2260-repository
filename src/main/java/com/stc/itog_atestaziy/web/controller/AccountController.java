package com.stc.itog_atestaziy.web.controller;

import com.stc.itog_atestaziy.web.model.Account;
import com.stc.itog_atestaziy.web.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }
    //http:localhost:8080/accounts
    @GetMapping
    public String getAllAccounts(Model model) {
        List<Account> accounts = accountService.getAllAccounts();
        model.addAttribute("accounts", accounts);
        return "accounts";
    }
    //http:localhost:8080/accounts/by-password
    @GetMapping("/by-password")
    public String getAllByPassword(@Param("password") String password, Model model) {
        List<Account> accounts = accountService.getAllAccountByPassword(password);
        model.addAttribute("accounts", accounts);
        return "accounts";
    }
    //http:localhost:8080/accounts/by-email
    @GetMapping("/by-email")
    public String getAllByEmail(@Param("email") String email, Model model) {
        List<Account> accounts = accountService.getAllAccountByEmail(email);
        model.addAttribute("accounts", accounts);
        return "accounts";
    }
}

