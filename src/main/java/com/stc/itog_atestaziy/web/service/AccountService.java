package com.stc.itog_atestaziy.web.service;

import com.stc.itog_atestaziy.web.model.Account;

import java.util.List;

public interface AccountService {

    List<Account> getAllAccounts();
    List<Account> getAllAccountByPassword(String password);

    List<Account> getAllAccountByEmail(String email);
}

