package com.stc.itog_atestaziy.web.service;


import com.stc.itog_atestaziy.web.dto.SignUpForm;

public interface SignUpService {

    void signUp(SignUpForm dto);
}
