package com.stc.itog_atestaziy.web.service;

import com.stc.itog_atestaziy.web.dto.SignUpForm;
import com.stc.itog_atestaziy.web.model.Account;
import com.stc.itog_atestaziy.web.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignUpServiceImpl implements SignUpService {

    private final AccountRepository accountRepository;

    @Autowired
    public SignUpServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void signUp(SignUpForm dto) {
        Account account = Account.builder()
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .build();

        accountRepository.save(account);
        System.out.println("Аккаунт сохранен");
    }
}
